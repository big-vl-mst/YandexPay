import base64
import re
from marshmallow import validate, ValidationError
from marshmallow.validate import Validator


class isBase64(Validator):
    """Validate Base64.

    :param error: Error message to raise in case of a validation error.
    """

    BASE64_REGEX = re.compile(
        r"^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$",
        re.IGNORECASE | re.UNICODE,
    )

    default_message = "Not a valid Base64."

    def __init__(self, *, error: str | None = None):
        self.error = error or self.default_message  # type: str

    def _format_error(self, value: str) -> str:
        return self.error.format(input=value)

    def __call__(self, value: str) -> str:
        message = self._format_error(value)
        if not self.BASE64_REGEX.match(value):
            raise ValidationError(message)
        return value
