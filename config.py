from os import environ

BASE_URL = environ.get("BASE_URL", "https://api.cloudpayments.ru/")
PUBLIC_ID = environ.get("PUBLIC_ID", "test_api_00000000000000000000001")
API_SECRET = environ.get("API_SECRET", "test")
SERVICE = environ.get("SERVICE", "ServiceCloudPayments")
CONNECT_TIMEOUT = environ.get("CONNECT_TIMEOUT", 30)
REQUEST_TIMEOUT = environ.get("REQUEST_TIMEOUT", 30)
