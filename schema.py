import base64
import binascii
import marshmallow.validate
from enum import Enum
from marshmallow_dataclass import NewType, class_schema, dataclass
from typing import Optional, Union
from marshmallow_enum import EnumField
from validate import isBase64

IPv4 = NewType(
    "IPv4",
    str,
    validate=marshmallow.validate.Regexp(r"^([0-9]{1,3}\.){3}[0-9]{1,3}$"),
)
Base64 = NewType(
    "Base64",
    str,
    validate=isBase64(),
    # validate=marshmallow.validate.Regexp(
    # r"^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$"
    # ),
    # uncomment or simple validation
)

@dataclass
class Charge3DResponse:
    TransactionId: int
    PaReq: str
    GoReq: None
    AcsUrl: str
    ThreeDsSessionData: None
    IFrameIsAllowed: bool
    FrameWidth: None
    FrameHeight: None
    ThreeDsCallbackId: str
    EscrowAccumulationId: None

@dataclass
class BaseClassResponse:
    Message: Optional[str]
    Success: bool
    Model: Optional[dict]

class CurrencyEnum(Enum):
    """
    List of currencies
    """
    RUB = "RUB"
    USD = "USD"
    EUR = "EUR"
    GBP = "GBP"

@dataclass
class Charge:
    """
    Crypto payment
    """
    Amount: float
    Currency = EnumField(CurrencyEnum)
    Description: str
    CardCryptogramPacket: Base64
    PublicId: str

ChargeSchema = class_schema(Charge)
BaseClassResponseSchema = class_schema(BaseClassResponse)
