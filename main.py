import re
import asyncio
import base64
import json
import config as cfg
from aiohttp import TCPConnector, BasicAuth
from abstract_client import AbstractInteractionClient, InteractionResponseError
from schema import ChargeSchema, BaseClassResponseSchema


class PaymentsInteractionClient(AbstractInteractionClient):

    def __init__(
        self,
        connector: TCPConnector, 
        request_timeout: float,
        connect_timeout: float,
        service: str,
        base_url: str,
        api_secret: str,
        public_id: str,
    ):
        # if not api_secret and not public_id:
            # raise ValueError("Not set API_SECRET and PUBLIC_ID")
        super().__init__()
        self.CONNECTOR = connector

        self.REQUEST_TIMEOUT = request_timeout 
        self.CONNECT_TIMEOUT = connect_timeout
    
        self.SERVICE = service
        self.BASE_URL = base_url
        self.__API_SECRET = api_secret
        self.__PUBLIC_ID = public_id

    async def execute(self, interaction_method=None, endpoint_url=None, schema=None):
        # Method value check
        if not interaction_method:
            raise ValueError("Interaction method not set")
        if not endpoint_url:
            raise ValueError("Endpoint url not set")
        endpoint_url = self.endpoint_url(endpoint_url)
        schema_json = None
        match interaction_method:
            case "cryptogram":
                schema_json, class_ = await self.cryptogram(ChargeSchema, schema)
                schema_json = await self._normalize_card_cryptogram_packet(schema_json)
        try:
            response = await self.post(
                interaction_method,
                endpoint_url,
                # auth=self._get_auth(), # uncomment for auth
                json=schema_json,
                headers=self._get_headers()
            )
            response = BaseClassResponseSchema().load(response)
        except InteractionResponseError as e:
            print(e)
            await self.close()
            response = None
        return response

    def _get_headers(self, headers={}):
        headers = {
            "Content-Type": "application/json",
            "User-Agent": "curl/7.83.1",
            "Accept": "*/*",
            "Host": "127.0.0.1:8080",
            **headers
        }
        return headers

    async def cryptogram(self, cls, schema):
        # Set AccountId
        await self._set_account_id(schema)

        charge_schema = schema_obj = cls()
        if isinstance(schema, cls):
            schema_json = schema.dump(schema)
        else:
            schema_json = schema
            schema_obj = charge_schema.load(schema)
        return (schema_json, schema_obj)

    async def _set_account_id(self, object_):
        if isinstance(object_, dict) and not object_.get("PublicId"):
            object_["PublicId"] = self.__PUBLIC_ID
        else:
            if not hasattr(object_, "PublicId"):
                setattr(object_, "PublicId", self.__PUBLIC_ID)
        return object_

    async def _normalize_card_cryptogram_packet(self, object_):
        if hasattr(object_, "CardCryptogramPacket") and object_.CardCryptogramPacket:
            object_.CardCryptogramPacket = str(
                await self.decode_base64(object_.CardCryptogramPacket)
            )
        if isinstance(object_, dict) and object_.get("CardCryptogramPacket"):
            object_["CardCryptogramPacket"] = str(
                await self.decode_base64(object_.get("CardCryptogramPacket"))
            )
        return object_

    async def decode_base64(self, value):
        """
        Function decode base64 and validate
        """
        RE_BASE64 = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$"
        if re.search(RE_BASE64, value):
            value = base64.b64decode(value).decode("utf-8")
        return value

    def _get_auth(self):
        auth = BasicAuth(
            login=self.__API_SECRET,
            password=self.__PUBLIC_ID,
            encoding="utf-8"
        )
        return auth

async def main():
    # VARIABLE
    token = "XCJ7XFxcInR5cGVcXFwiOlxcXCJZYW5kZXhcXFwiLFxcXCJzaWduZWRNZXNzYWdlXFxcIjpcXFwie1xcXFxcXFwiZW5jcnlwdGVkTWVzc2FnZVxcXFxcXFwiOlxcXFxcXFwieHFwQWlTMkw3MUJaTmdINTE0QVFEd09WYXdKRjRnSFhGOFArRUNJRlJxRkhsRE1SdHhIc085aE5RU2VlZ1Nzc1JkRE1sQkl5T09iWTVkcUkzaXdYOTlVS1lQNnFGRCt0S0VZSlFrVWRpS3loWkN3Z1VzVmRIQmxGUUEraWlYVkxmN0RaNVdDSWFIanBsNG1ja3JHZURnNFhHRElYNEZCMEJvckxxb2NiRExjbDBKWmkyenprTnRuOUZETFBTQXMxcWJURU1kYjNUQVMwaURBSWt1QXk1REdKMys0QXY5UFd2SWxsVzRMUmRRMzRyUjhNUHN6SnhxOVhhZ3cvamVLVWdseVVFUlFnaTVjblZXSUI5OTJ5UGg5VUZnTnVDUUJjK0pXTE16dU9JS0t4RmlWSzZWQlNzdUhwRFdyU1pxTW9sTjZQSWVOdkVUeFEzNGcrTy91NEtpd1dkM0lHL3BiNWUwRlliem4vZ1d6bERTUHNxTlN1QjcxM3FaREhDSTdlRkI3aDdpUFRkay9XZDc4VnY3VmxnNG9WUWRNV0NiZ1NqdFdEYW1LZXEvT01pVkRXNWozNkNlYlJRV3hCOC9YRmo0bkFJbkhJam9VVUtzRVE1Z2YwMG45LzQ4UlVOVkNiUnI2cXlrdnNmbkQwWFA1VjRPSk9lSWhBWk4yQ0FnR3hnckdDNU1pYmZqQWYrRC9FbnVuSHdPdnRtSTZLUUFzR3Y5UWdyUkM4c3hUZXlrN09UOXZVQ3pLMkRJUkRZeUN0dmxvR2FsUnExUFJkSldRWFxcXFxcXFwiLFxcXFxcXFwidGFnXFxcXFxcXCI6XFxcXFxcXCJMVHg2L0hBOWlXYVp3YllhRk4xajlhRE9QcDJQQmxSMmlCTVVCUTd6eVVnPVxcXFxcXFwiLFxcXFxcXFwiZXBoZW1lcmFsUHVibGljS2V5XFxcXFxcXCI6XFxcXFxcXCJCSEhCY1Q0U3ZGZ3hNSzE0T3ozL2RrL3VpQ0wybTRqZVRGREVjb1lIWHQ1Z0F6MndGVkVudlJENGZIQXJrYklPY3J5OW5sVVlIV2dUNEdpY0VsOXFrWFk9XFxcXFxcXCJ9XFxcIixcXFwicHJvdG9jb2xWZXJzaW9uXFxcIjpcXFwiRUN2MlxcXCIsXFxcInNpZ25hdHVyZVxcXCI6XFxcIk1FVUNJQ3l5elduQ0VmMmlIbFVzekR6dmJBeC9xay9zTG1iVGFPV1BWRXExaHIyOUFpRUEwbGZaODVwQ29mWWh4Vlg5NzFYdHNoeXNhd2k3K0tFZThacFBWbFYvTWQ0PVxcXCIsXFxcImludGVybWVkaWF0ZVNpZ25pbmdLZXlcXFwiOntcXFwic2lnbmVkS2V5XFxcIjpcXFwie1xcXFxcXFwia2V5VmFsdWVcXFxcXFxcIjpcXFxcXFxcIk1Ga3dFd1lIS29aSXpqMENBUVlJS29aSXpqMERBUWNEUWdBRXFZTmVQdDZCUGdDdjVKeGZPOWRGMnZyU3FtbnA0TWhlL3ZGK1hPK0RldmJzNi9LVnBWVm9URDhMTGNBbzRUWmg2SXVPRFZuVnBIclRPYmhnM0hKVkpBPT1cXFxcXFxcIixcXFxcXFxcImtleUV4cGlyYXRpb25cXFxcXFxcIjpcXFxcXFxcIjE3NjQ5NTA4OTIwMDBcXFxcXFxcIn1cXFwiLFxcXCJzaWduYXR1cmVzXFxcIjpbXFxcIk1FUUNJRFJzbE1XN3dOWmJwcVZ3L2REN2hEUWgzMGhHaHFmamZXVEJ2Yzd6QVlKU0FpQUdBdmpBc2xBMkF4d2RBRXVPZmFjRnI2RGFFNXlpaVV1VXRNNkRVcmVaWWc9PVxcXCJdfX1cIg=="
    payment = {
        "Amount": 100,
        # "IpAddress": "123.123.123.123",
        "CardCryptogramPacket": token,
        "Description": "A basket of oranges"
    }
    # EXAMPLE
    cryptogram = await PaymentsInteractionClient(
        connector=TCPConnector(force_close=True), 
        request_timeout=cfg.REQUEST_TIMEOUT,
        connect_timeout=cfg.CONNECT_TIMEOUT,
        service=cfg.SERVICE,
        base_url=cfg.BASE_URL,
        api_secret=cfg.API_SECRET,
        public_id=cfg.PUBLIC_ID,
    ).execute(
        interaction_method="cryptogram",
        endpoint_url="payments/charge",
        schema=payment
    )
if __name__ == "__main__":
    asyncio.run(main())

